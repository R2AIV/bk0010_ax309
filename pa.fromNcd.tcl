
# PlanAhead Launch Script for Post PAR Floorplanning, created by Project Navigator

create_project -name AX309BK -dir "F:/cpld_/AX309BK/planAhead_run_1" -part xc6slx9ftg256-3
set srcset [get_property srcset [current_run -impl]]
set_property design_mode GateLvl $srcset
set_property edif_top_file "F:/cpld_/AX309BK/bk0010_wxeda.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {F:/cpld_/AX309BK} {ipcore_dir} }
add_files [list {ipcore_dir/bootrom.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/vram.ncf}] -fileset [get_property constrset [current_run]]
set_property target_constrs_file "Constr.ucf" [current_fileset -constrset]
add_files [list {Constr.ucf}] -fileset [get_property constrset [current_run]]
link_design
read_xdl -file "F:/cpld_/AX309BK/bk0010_wxeda.ncd"
if {[catch {read_twx -name results_1 -file "F:/cpld_/AX309BK/bk0010_wxeda.twx"} eInfo]} {
   puts "WARNING: there was a problem importing \"F:/cpld_/AX309BK/bk0010_wxeda.twx\": $eInfo"
}
